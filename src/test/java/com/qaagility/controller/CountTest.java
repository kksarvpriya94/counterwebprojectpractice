package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
 
public class CountTest {

 

@Test
public void testCalc() {
	Count cnt = new Count();
	assertEquals("Result", cnt.dividenew(1,0), Integer.MAX_VALUE);
}
@Test
public void testCalcHappyTest() {
	Count cnta = new Count();
	assertEquals("Result", cnta.dividenew(4,2), 2);
}
@Test
public void testCalcNegativeTestWithASmall() {
	Count cntb = new Count();
	assertEquals("Result", cntb.dividenew(2,4), 0);
}
@Test
public void testCalcNegativeTestWithAZero() {
	Count cntc = new Count();
	assertEquals("Result", cntc.dividenew(0,2), 0);
}
	
	
    }
