package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders; 
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



public class BaseControllerTest{


	
    private MockMvc mockMvc ;   

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        
        this.mockMvc = MockMvcBuilders.standaloneSetup(new BaseController()).build();
    }    
    
@Test
public void testCalc() {
	Calcmul myCalc = new Calcmul();
	assertEquals("Result", myCalc.mul(), 18);
}


@Test
public void welcomeTest() throws Exception {

    this.mockMvc.perform(get("/"))
            .andExpect(status().isOk());
   

}


@Test
public void welcomeNameTest() throws Exception {

    this.mockMvc.perform(get("/Arora"))
            .andExpect(status().isOk());
   

}


	
    }
